<?php return [
    'plugin' => [
        'name' => 'Utilty',
        'description' => 'Utility',
    ],
    'validation' => [
        'zipcode' => 'Write correctly the zipcode',
    ],
    'cache' => 'Cache',
    'redis' => 'Redis',
    'permissions' =>
        [
            'plugin_access' => 'Plugin access'
        ]
];