<?php namespace Entopancore\Utility\Behaviors;

use Db;
use Str;
use Lang;
use Flash;
use Event;
use Input;
use Redirect;
use Backend\Classes\ControllerBehavior;
use ApplicationException;

class MediaController extends ControllerBehavior
{
    protected $requiredProperties = ['mediaConfig'];
    public $controller;
    public $folder;
    public $type;
    public $foreignKey;

    public function __construct($controller)
    {
        parent::__construct($controller);
        $this->controller = $controller;
        if ($controller->formConfig) {
            $config = $this->makeConfig($controller->formConfig);
            $modelClass = explode('\\', $config->modelClass);
            $name = array_pop($modelClass);
            $name = snake_case($name);
            $this->folder = ($this->controller->mediaConfig['folder']) ?? $name;
            $this->type = ($this->controller->mediaConfig['type']) ?? 'simple';

        } else {
            if (class_basename($controller) == 'Settings') {
                $this->folder = $this->controller->mediaConfig['folder'];
                setStorageRoot($this->folder);
            }
        }
    }

    public function formExtendModel($model)
    {

        if ($this->type == 'nested') {
            $folder = $this->folder . '/' . $model->id;
        }
        if ($this->type == 'simple') {
            $folder = $this->folder;
        }
        if ($this->type == 'foreign') {
            if (is_integer($this->controller->mediaConfig['foreign'])) {
                $foreignKey = $this->controller->mediaConfig['foreign'];
            } else {
                $foreignKey = $model->{$this->controller->mediaConfig['foreign']};
            }
            $folder = $this->folder . '/' . $foreignKey;
        }
        if (isset($folder)) {
            setStorageRoot($folder);
        }


    }

}