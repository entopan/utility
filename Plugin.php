<?php namespace Entopancore\Utility;

use Jenssegers\Date\Date;
use System\Classes\PluginBase;
use Validator;
use App;
use Event;

/**
 * Utility Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function boot()
    {
        if (!App::runningInBackend()) {
            return;
        }

        Date::setLocale(config("app.locale"));

        Event::listen('system.console.mirror.extendPaths', function ($paths) {
            $paths->directories = array_merge($paths->directories, ['assets/css', 'assets/js', 'sitemap', storage_path('api-docs')]);
            return $paths->directories;
        });
        Event::listen('backend.page.beforeDisplay', function ($controller, $action, $params) {
            if (isset($controller->implement)) {
                if (!in_array('Entopancore\Utility\Behaviors\MediaController', $controller->implement)) {
                    resetStorageRoot(request());
                }
            } else {
                resetStorageRoot(request());
            }
        });
    }


    public function register()
    {
        \App::register('Entopancore\Utility\Http\UtilityServiceProvider');
        $this->registerConsoleCommand('entopancore.media', 'Entopancore\Utility\Console\MediaCommand');
        $this->registerConsoleCommand('entopancore.mailbrandsetting', 'Entopancore\Utility\Console\MailBrandSettingCommand');
    }


    public function registerFormWidgets()
    {

        return [
            'Entopancore\Utility\FormWidgets\DateRangePicker' => [
                'label' => 'Daterange picker',
                'code' => 'daterangepicker'
            ],
            'Entopancore\Utility\FormWidgets\Gap' => [
                'label' => 'Gap',
                'code' => 'gap'
            ]
        ];

    }

    public function registerMarkupTags()
    {
        return [
            'functions' => [
                'config' => [$this, 'config']
            ]
        ];
    }


    public function config($text)
    {
        return config($text);
    }


}
