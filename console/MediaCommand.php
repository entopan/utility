<?php namespace Entopancore\Utility\Console;

use Illuminate\Console\Command;

class MediaCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'entopancore:media';

    /**
     * @var string The console command description.
     */
    protected $description = 'Change media library file';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        \File::copy(plugins_path()."/entopancore/utility/console/templates/modules/system/classes/MediaLibrary.php", base_path()."/modules/system/classes/MediaLibrary.php");
        \File::copy(plugins_path()."/entopancore/utility/console/templates/modules/backend/widgets/mediamanager/partials/_sorting.htm", base_path()."/modules/backend/widgets/mediamanager/partials/_sorting.htm");
        $this->info('File moved');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}