<?php namespace Entopancore\Utility\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\Storage;

class MailBrandSettingCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'entopancore:mailbrandsetting';

    /**
     * @var string The console command description.
     */
    protected $description = 'Destroy custom less';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        file_put_contents(base_path("modules/system/models/mailbrandsetting/custom.less"), null);
        $this->info('Less destroyed');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}