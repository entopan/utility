<?php namespace Entopancore\Utility\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Carbon\Carbon;

class DateRangePicker extends FormWidgetBase
{
    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'daterangepicker';

    protected $format = 'Y-m-d';

    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('daterangepicker');
    }


    public function prepareVars()
    {

        $format = $this->getFormat();
        $this->vars["disabled"] = $this->formField->disabled;
        $this->vars['format'] = $this->convertPHPToMomentFormat($format);
        $this->vars['name'] = $this->formField->getName();
        $this->vars['minDate'] = $this->getMinDate();
        $this->vars['maxDate'] = $this->getMaxDate();
        if ($value = $this->getLoadValue()) {
            $value = explode("|", $value);
            foreach ($value as $k => $v) {
                $v = date_format(date_create($v), 'U');
                $value[$k] = Carbon::createFromTimestamp($v)->format($this->getFormat());
            }
            $value = implode(" | ", $value);
        }
        $this->vars['value'] = $value;
    }

    public function loadAssets()
    {
        $this->addCss("daterangepicker.css");
        $this->addJs("daterangepicker.js");
    }


    private function getMinDate()
    {
        if ($this->formField->getConfig("minDate")) {
            $date = Carbon::createFromTimestamp($this->formField->getConfig("minDate"));
        } else {
            $date = Carbon::now()->subYears(10);
        }
        return $date->format($this->getFormat());
    }

    private function getMaxDate()
    {
        if ($this->formField->getConfig("maxDate")) {
            $date = Carbon::createFromTimestamp($this->formField->getConfig("maxDate"));
        } else {
            $date = Carbon::now()->addYears(10);
        }
        return $date->format($this->getFormat());
    }

    private function getFormat()
    {
        return ($this->formField->getConfig("format")) ?? $this->format;
    }

    private function convertPHPToMomentFormat($format)
    {
        $replacements = [
            'd' => 'DD',
            'D' => 'ddd',
            'j' => 'D',
            'l' => 'dddd',
            'N' => 'E',
            'S' => 'o',
            'w' => 'e',
            'z' => 'DDD',
            'W' => 'W',
            'F' => 'MMMM',
            'm' => 'MM',
            'M' => 'MMM',
            'n' => 'M',
            't' => '', // no equivalent
            'L' => '', // no equivalent
            'o' => 'YYYY',
            'Y' => 'YYYY',
            'y' => 'YY',
            'a' => 'a',
            'A' => 'A',
            'B' => '', // no equivalent
            'g' => 'h',
            'G' => 'H',
            'h' => 'hh',
            'H' => 'HH',
            'i' => 'mm',
            's' => 'ss',
            'u' => 'SSS',
            'e' => 'zz', // deprecated since version 1.6.0 of moment.js
            'I' => '', // no equivalent
            'O' => '', // no equivalent
            'P' => '', // no equivalent
            'T' => '', // no equivalent
            'Z' => '', // no equivalent
            'c' => '', // no equivalent
            'r' => '', // no equivalent
            'U' => 'X',
        ];
        $momentFormat = strtr($format, $replacements);
        return $momentFormat;
    }


}