<?php namespace Entopancore\Utility\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Carbon\Carbon;

class Gap extends FormWidgetBase
{
    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'gap';


    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('gap');
    }


    public function prepareVars()
    {

        $this->vars['height'] = $this->getHeight();
    }

    private function getHeight()
    {
        if ($height = $this->formField->getConfig("height")) {
            return $height;
        }
        return 0;
    }

}