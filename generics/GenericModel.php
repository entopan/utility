<?php namespace Entopancore\Utility\Generics;
/**
 * Class GenericModel
 * @package Entopancore\Utility\Generics
 */
class GenericModel
{
    /**
     * GenericModel constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $property
     * @return mixed
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    /**
     * @param $property
     * @param $value
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    /**
     * @return array
     */
    public function get()
    {
        $empties = [];
        $values = (array)$this;
        foreach ($values as $k => $v) {
            if (is_null($v)) {
                array_push($empties, $k);
            }
        }
        return (array)$values;
    }


    protected function getFrontendLink($path)
    {
        return implode("", [config("app.url_frontend"), $path]);
    }

    protected function getBackendLink($path)
    {
        return \Backend::url($path);
    }
}

