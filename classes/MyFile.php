<?php namespace Entopancore\Utility\Classes;

use System\Models\File;

class MyFile extends File
{
    public $appends = [
        "image_header",
        "image_header_default",
        "image_default",
        "image_grid",
        "image_medium",
        "image_facebook",
        "image_twitter",
        "image_thumb"
    ];

    public function getImageHeaderAttribute()
    {
        return $this->getAttachOptimizePath('extra');
    }

    public function getImageHeaderDefaultAttribute()
    {
        return $this->getPath();
    }

    public function getImageDefaultAttribute()
    {
        return $this->getPath();
    }

    public function getImageGridAttribute()
    {
        return $this->getAttachOptimizePath('small');
    }

    public function getImageMediumAttribute()
    {
        return $this->getAttachOptimizePath('medium');
    }

    public function getImageFacebookAttribute()
    {
        return $this->getAttachOptimizePath('facebook');
    }

    public function getImageTwitterAttribute()
    {
        return $this->getAttachOptimizePath('facebook');
    }

    public function getImageThumbAttribute()
    {
        return $this->getAttachOptimizePath('mini');
    }

    private function getAttachOptimizePath($size)
    {
        $type = explode('/', $this->getContentType())[0];

        if ($type == 'image') {
            if ($arrayPath = explode('uploads/public/', $this->getPath())) {
                if (count($arrayPath) == 2) {
                    $slug = config("cms.storage.optimizeArray")[$size];
                    return $this->getOptimizePath($slug, 'public/' . $arrayPath[1]);
                }
            }
        }

        return null;
    }

    private function getOptimizePath($slug, $folder)
    {
        $rand = rand(0, 1);
        $domain = config('cms.storage.media.optimize')[$rand];
        return str_replace(' ', '%20', $domain . '/uploads=' . $slug . '/' . $folder);
    }
}
