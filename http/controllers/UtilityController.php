<?php namespace Entopancore\Utility\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class UtilityController extends Controller
{
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function clearCache()
    {
        \Cache::store('api')->flush();
        return getSuccessResult(null, "Cache pulita con successo");
    }

    public function previewMail($layout = 1, $view = null)
    {
        return mailPreview($layout, $view);
    }


}