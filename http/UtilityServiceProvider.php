<?php

namespace Entopancore\Utility\Http;

use Illuminate\Support\ServiceProvider;

class UtilityServiceProvider extends ServiceProvider
{

    public function register()
    {
        require_once 'helpers/UtilityHelpers.php';
    }

}
