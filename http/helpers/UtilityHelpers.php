<?php


/*
 *
 * FOLDERABLE
 *
 */

if (!function_exists('checkStorage')) {
    function checkStorage()
    {
        return session('storageFolderable');
    }
}

if (!function_exists('resetStorageRoot')) {
    function resetStorageRoot(\Illuminate\Http\Request $request)
    {
        if ($request->session()->has('storageFolderable')) {
            $request->session()->forget('storageFolderable');
        }
    }
}

if (!function_exists('fixDoubleNumber')) {

    function fixDoubleNumber($value, $approximate = false)
    {
        $value = str_replace(",", ".", $value);
        if ($approximate and $approximate != "nothing") {
            if ($approximate == "decimal") {
                $x = $value - floor($value);
                if ($x > 0.5) {
                    $value = round($value, 0, PHP_ROUND_HALF_UP);
                } else {
                    $value = round($value, 0, PHP_ROUND_HALF_DOWN);
                }
            } elseif ($approximate == "integer") {
                $x = $value - floor($value);

                if ($value > 1 and $value < 10) {
                    if ($x >= 0.6) {
                        $value = round($value, 0, PHP_ROUND_HALF_UP);
                    } else {
                        $value = round($value, 0, PHP_ROUND_HALF_DOWN);
                    }
                } elseif ($value > 10 and $value < 100) {
                    if ($x >= 0.6) {
                        $value = round($value, 0, PHP_ROUND_HALF_UP);
                    } else {
                        $value = round($value, 0, PHP_ROUND_HALF_DOWN);
                    }
                } elseif ($value > 100 and $value < 1000) {
                    if ($x >= 0.6) {
                        $value = round($value, 0, PHP_ROUND_HALF_UP);
                    } else {
                        $value = round($value, 0, PHP_ROUND_HALF_DOWN);
                    }
                } elseif ($value > 1000 and $value < 10000) {
                    if ($x >= 0.6) {
                        $value = round($value, 2, PHP_ROUND_HALF_UP);
                    } else {
                        $value = round($value, 2, PHP_ROUND_HALF_DOWN);
                    }
                    $dozens = substr($value, -4);
                    if ($dozens > 5) {
                        $value = $value + 5;
                    }
                    if ($dozens <= 5) {
                        $value = $value - 5;
                    }
                    $value = (int)$value;
                } elseif ($value > 10000 and $value < 100000) {
                    $integerValue = (int)$value;
                    $dozens = substr($integerValue, -2);
                    if ($dozens > 50) {
                        $integerValue = $integerValue + (100 - $dozens);
                    }
                    if ($dozens <= 50) {
                        $integerValue = $integerValue - $dozens;
                    }
                    $value = $integerValue;
                }

            }

            return $value;
        } else {
            return round($value, 2);
        }


    }
}

/*
 *
 *  $x = $value - floor($value);
                    if ($x >= 0.6) {
                        $value = round($value, 0, PHP_ROUND_HALF_UP);
                    } else {
                        $value = round($value, 0, PHP_ROUND_HALF_DOWN);
                    }
 */

if (!function_exists('mailPreview')) {

    function mailPreview($layout, $view, $data = [])
    {
        $view = mailPath($view);
        $mail = \October\Rain\Mail\MailParser::parse(view($view, $data));
        $html = $mail["html"];
        $layout = \System\Models\MailLayout::find($layout);
        $template = new \System\Models\MailTemplate();
        $template->layout = $layout;
        $template->content_html = $html;
        return \System\Classes\MailManager::instance()->renderTemplate($template);
    }
}


if (!function_exists('mailPath')) {

    function mailPath($view)
    {
        $customView = str_replace("::", ".", $view);
        if (\View::exists($customView)) {
            return $customView;
        }
        return $view;
    }
}


if (!function_exists('setStorageRoot')) {
    function setStorageRoot($folder)
    {
        session(['storageFolderable' => $folder]);
    }
}


if (!function_exists('fromObjectToArray')) {
    function fromObjectToArray($object)
    {
        return json_decode(json_encode($object), true);
    }
}

if (!function_exists('fromCsvToArray')) {
    function fromCsvToArray($string)
    {
        return $string ? explode(',', $string) : [];
    }
}

if (!function_exists('prepareFilters')) {

    function prepareFilters($array)
    {
        $filters = [];
        foreach ($array as $k => $a) {
            $filters[$k] = fromCsvToArray($a);
        }
        return $filters;
    }
}


if (!function_exists('fromObjectToArraySingle')) {
    function fromObjectToArraySingle($object)
    {
        $array = json_decode(json_encode($object), true);
        if (isset($array[0])) {
            return $array[0];
        } else {
            return false;
        }
    }
}


if (!function_exists('generateUniqueCode')) {
    function generateUniqueCode($length, $model = null, $field = null)
    {
        $random = strtoupper(str_random($length));
        if (!$model) {
            return $random;
        }
        $newModel = new $model;
        if ($newModel->where($field, '=', $random)->count() != 0) {
            generateUniqueCode($length, $model, $field);
        } else {
            return $random;
        }
    }

}


if (!function_exists('removeAccents')) {

    function removeAccents($str)
    {
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η');
        return str_replace($a, $b, $str);
    }
}


if (!function_exists('getDirectoryFiles')) {

    function getDirectoryFiles($directory)
    {
        $result = array();
        $files = scandir($directory);
        foreach ($files as $file) {
            if ($file === '.' or $file === '..' or $file === '.DS_Store' or $file === '.git') continue;
            array_push($result, (pathinfo($file)));
        }
        return $result;
    }
}


if (!function_exists('copyDirectory')) {

    function copyDirectory($source, $target)
    {
        $files = getDirectoryFiles($source);
        if (!\File::isDirectory($target)) {
            \File::makeDirectory($target);
        }

        foreach ($files as $file) {
            $sourceBasePath = $source . "/" . $file["basename"];
            $targetBasePath = $target . "/" . $file["basename"];
            if (!\File::isDirectory($sourceBasePath)) {
                \File::copy($sourceBasePath, $targetBasePath);
            } else {
                copyDirectory($sourceBasePath, $targetBasePath);
            }
        }
    }
}

if (!function_exists('moveFile')) {

    function moveFile($from, $to)
    {
        if (\Storage::disk(config('filesystems.default'))->exists($from)) {
            if (!\Storage::disk(config('filesystems.default'))->exists($to)) {
                \Storage::disk(config('filesystems.default'))->move($from, $to);
            }
        }
    }
}

if (!function_exists('copyFile')) {

    function copyFile($from, $to)
    {
        if (\Storage::disk(config('filesystems.default'))->exists($from)) {
            if (!\Storage::disk(config('filesystems.default'))->exists($to)) {
                \Storage::disk(config('filesystems.default'))->copy($from, $to);
            }
        }
    }
}


if (!function_exists("strReplaceLast")) {

    function strReplaceLast($search, $replace, $str)
    {
        if (($pos = strrpos($str, $search)) !== false) {
            $search_length = strlen($search);
            $str = substr_replace($str, $replace, $pos, $search_length);
        }
        return $str;
    }
}

if (!function_exists("buildCartesian")) {

    function buildCartesian($input)
    {
        $result = array();

        while (list($key, $values) = each($input)) {
            // If a sub-array is empty, it doesn't affect the cartesian product
            if (empty($values)) {
                continue;
            }

            // Seeding the product array with the values from the first sub-array
            if (empty($result)) {
                foreach ($values as $value) {
                    $result[] = array($key => $value);
                }
            } else {
                // Second and subsequent input sub-arrays work like this:
                //   1. In each existing array inside $product, add an item with
                //      key == $key and value == first item in input sub-array
                //   2. Then, for each remaining item in current input sub-array,
                //      add a copy of each existing array inside $product with
                //      key == $key and value == first item of input sub-array

                // Store all items to be added to $product here; adding them
                // inside the foreach will result in an infinite loop
                $append = array();

                foreach ($result as &$product) {
                    // Do step 1 above. array_shift is not the most efficient, but
                    // it allows us to iterate over the rest of the items with a
                    // simple foreach, making the code short and easy to read.
                    $product[$key] = array_shift($values);

                    // $product is by reference (that's why the key we added above
                    // will appear in the end result), so make a copy of it here
                    $copy = $product;

                    // Do step 2 above.
                    foreach ($values as $item) {
                        $copy[$key] = $item;
                        $append[] = $copy;
                    }

                    // Undo the side effecst of array_shift
                    array_unshift($values, $product[$key]);
                }

                // Out of the foreach, we can add to $results now
                $result = array_merge($result, $append);
            }
        }

        return $result;
    }

}

if (!function_exists("currentTimeMillis")) {

    function currentTimeMillis()
    {
        list($usec, $sec) = explode(" ", microtime());
        return round(((float)$usec + (float)$sec) * 1000);
    }
}

if (!function_exists("generateQrCodeFromString")) {

    function generateQrCodeFromString($string)
    {
        return base64_encode(\QrCode::margin(0)->size(400)->generate($string));
    }
}