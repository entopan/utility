<?php

/**
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     basePath="/api/v1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="API Documentation",
 *         description="This is the API Documentation for the project.",
 *         @SWG\Contact(
 *             email="passantifrancesco@gmail.com"
 *         ),
 *         @SWG\License(
 *             name="Apache 2.0",
 *             url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *         )
 *     )
 * )
 */

Route::group(
    [
        'prefix' => 'api/v1/public/utility',
        'middleware' => [
            'api'
        ],
    ], function () {
    Route::get('cache-clear', ['uses' => 'Entopancore\Utility\Http\Controllers\UtilityController@clearCache', 'as' => 'clearCache']);
    Route::get('preview-mail/{id}/{view}', ['uses' => 'Entopancore\Utility\Http\Controllers\UtilityController@previewMail', 'as' => 'previewMail']);
});






