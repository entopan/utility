<?php namespace Entopancore\Utility\Traits;


/**
 * Created by PhpStorm.
 * User: annaubaldino
 * Date: 27/01/17
 * Time: 16:43
 */
trait Folderable
{

    public $mandatoryImages = ["image_header", "image_header_default", "image_default", "image_grid", "image_facebook", "image_twitter", "image_thumb"];

    public static function bootFolderable()
    {

        if (!property_exists(get_called_class(), 'folderable')) {
            throw new \Exception(sprintf(
                'You must define a $folderable property in %s to use the Folderable trait.', get_called_class()
            ));
        }

        static::extend(function ($model) {

            $arrayIntersect = array_intersect($model->mandatoryImages, array_keys($model->folderable["images"]));
            if (count($arrayIntersect) != count($model->mandatoryImages)) {
                info(get_called_class());
            }
            $folder = ($model->folderable["folder"]) ?? snake_case(class_basename($model));
            $type = $model->folderable["type"];
            $foreignKey = ($model->folderable["foreign"]) ?? null;

            foreach ($model->folderable["images"] as $k => $image) {
                if (is_null($image)) {
                    continue;
                }
                $model->append([$k]);
                $value = explode("|", $image);
                $img = $value[0];

                if (count($value) == 1) {
                    $model->addDynamicMethod('get' . ucfirst(camel_case($k)) . 'Attribute', function ($value) use ($model, $img, $folder, $type, $foreignKey) {
                        if (isset($model->folderable["custom"])) {
                            return $model->{$img};
                        }
                        if ($model->{$img}) {
                            if ($type == "nested") {
                                return self::getMediaPath($folder . '/' . $model->id . $model->{$img});
                            } else if ($type == "simple") {
                                return self::getMediaPath($folder . $model->{$img});
                            } else if ($type == "foreign") {
                                return self::getMediaPath($folder . '/' . $model->{$foreignKey} . $model->{$img});
                            }
                        } else {
                            return null;
                        }
                    });
                } else if (count($value) == 2) {
                    $size = $value[1];
                    $size = config("cms.storage.optimizeArray")[$size];

                    $model->addDynamicMethod('get' . ucfirst(camel_case($k)) . 'Attribute', function ($value) use ($size, $model, $img, $folder, $type, $foreignKey) {
                        if (isset($model->folderable["custom"])) {
                            return str_replace('/media/', '/media=' . $size . '/', $model->{$img});
                        }
                        if ($model->{$img}) {
                            if ($type == "nested") {
                                return self::getOptimizePath($size, $folder . '/' . $model->id . $model->{$img});
                            } else if ($type == "simple") {
                                return self::getOptimizePath($size, $folder . $model->{$img});
                            } else if ($type == "foreign") {
                                return self::getOptimizePath($size, $folder . '/' . $model->{$foreignKey} . $model->{$img});
                            }
                        } else {
                            return null;
                        }
                    });
                }


            }
        });


    }


    private function getFolderablePath($idOrForeignKey, $path, $type, $folder)
    {

        if ($type == "nested") {
            return self::getMediaPath($folder . '/' . $idOrForeignKey . $path);
        } else if ($type == "simple") {
            return self::getMediaPath($folder . $path);
        } else if ($type == "foreign") {
            return self::getMediaPath($folder . '/' . $idOrForeignKey . $path);
        }


    }


    public static function getMediaPath($folder)
    {
        return str_replace(' ', '%20', config('cms.storage.media.path') . '/' . $folder);
    }

    public static function getOptimizePath($slug, $folder)
    {
        $rand = rand(0, 1);
        $domain = config('cms.storage.media.optimize')[$rand];
        return str_replace(' ', '%20', $domain . '/media=' . $slug . '/' . $folder);
    }

}