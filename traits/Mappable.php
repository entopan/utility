<?php namespace Entopancore\Utility\Traits;

trait Mappable
{

    public static function bootMappable()
    {
        if (!property_exists(get_called_class(), 'mappable')) {
            throw new \Exception(sprintf(
                'You must define a $mappable property in %s to use the Mappable trait.', get_called_class()
            ));
        }
    }

    public static function nearestItems($take = null, $lat, $lng, $maxDistance = null, $condition = "1=1")
    {
        $R = 6371;  // earth's mean radius, km

        $class = get_called_class();
        $model = $modelDefault = new $class;
        $table = $model->table;
        $latLabel = $model->mappable["lat"];
        $lngLabel = $model->mappable["lng"];
        $maxLat = $lat + rad2deg($maxDistance / $R);
        $minLat = $lat - rad2deg($maxDistance / $R);
        $maxLon = $lng + rad2deg(asin($maxDistance / $R) / cos(deg2rad($lat)));
        $minLon = $lng - rad2deg(asin($maxDistance / $R) / cos(deg2rad($lat)));


        $sql = "SELECT *,
                   acos(sin(?)*sin(radians($latLabel)) + cos(?)*cos(radians($latLabel))*cos(radians($lngLabel)-?)) * ? As D
            From (
                Select *
                From $table
                Where $latLabel Between ? And ?
                  And $lngLabel Between ? And ?
                  And $condition
            ) As FirstCut
            Where acos(sin(?)*sin(radians($latLabel)) + cos(?)*cos(radians($latLabel))*cos(radians($lngLabel)-?)) * ? < ?
            ORDER BY D ASC";
        $sql .= " LIMIT ? ";

        $params = [deg2rad($lat), deg2rad($lat), deg2rad($lng), $R, $minLat, $maxLat, $minLon, $maxLon, deg2rad($lat), deg2rad($lat), deg2rad($lng), $R, $maxDistance, $take];
        $result = \DB::select($sql, $params);

        $result = json_decode(json_encode($result), true);

        $jsonableFields = $modelDefault->jsonable;

        foreach ($result as $k => $r) {
            foreach ($jsonableFields as $field) {
                $result[$k][$field] = json_decode($r[$field]);
            }
        }
        return $result;


    }


}