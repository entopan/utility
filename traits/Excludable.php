<?php namespace Entopancore\Utility\Traits;

use Exception;
use Entopancore\Context\Models\Context;
use Entopancore\Context\Models\Contextable as ContextableModel;

trait Excludable
{
    public function scopeExclude($query, $columns)
    {
        return $query->select(array_diff($this->getTableColumns(), (array)$columns));
    }


    private function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

}
