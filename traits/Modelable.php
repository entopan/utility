<?php namespace Entopancore\Utility\Traits;

use Entopancore\Utility\Classes\GoogleMaps;

/**
 * Created by PhpStorm.
 * User: annaubaldino
 * Date: 27/01/17
 * Time: 16:43
 */
trait Modelable
{

    public static function autocomplete($field, $value, $type = "middle", $where = null, $select = "*", $items = 10, $with = null, $skip = 0, $get = false)
    {
        $class = get_called_class();
        $model = $modelDefault = new $class;
        $model = $modelDefault->getWithConfiguration($model, $with);

        if (!is_array($value)) {
            $value = strtolower(removeAccents($value));
        }

        if (!is_array($value)) {
            switch ($type) {
                case "begin":
                    $value = $value . '%';
                    break;
                case "end":
                    $value = '%' . $value;
                    break;
                case "middle":
                    $value = '%' . $value . '%';
                    break;
                case "multiple":
                    $model = $modelDefault->getWhereConfiguration($model, $where);
                    $terms = explode(" ", $value);
                    $x = '';

                    foreach ($terms as $term) {
                        $term = strtolower(removeAccents($term));
                        if ($term === end($terms)) {
                            $x .= $field . ' LIKE ' . '\'%' . $term . '%\'';
                        } else {
                            $x .= $field . ' LIKE ' . '\'%' . $term . '%\' AND ';
                        }
                    }
                    return $model->whereRaw($x)->select($select)->take($items)->get();
                    break;
                case "multiple_or":
                    $model = $modelDefault->getWhereConfiguration($model, $where);
                    $terms = explode(" ", $value);
                    $x = '';

                    foreach ($terms as $term) {
                        $term = strtolower(removeAccents($term));
                        if ($term === end($terms)) {
                            $x .= $field . ' LIKE ' . '\'%' . $term . '%\'';
                        } else {
                            $x .= $field . ' LIKE ' . '\'%' . $term . '%\' OR ';
                        }
                    }
                    return $model->whereRaw($x)->select($select)->take($items)->get();
                    break;
            }
        } else {
            switch ($type) {
                case "beginEnd":
                    $value = $value[0] . '%' . $value[1];
                    break;
                case "beginMiddle":
                    $value = $value[0] . '%' . $value[1] . '%';
                    break;
                case "middleEnd":
                    $value = '%' . $value[0] . '%' . $value[1];
                    break;
                case "beginMiddleEnd":
                    $value = $value[0] . '%' . $value[1] . '%' . $value[2];
                    break;
            }
        }

        $model = $modelDefault->getWhereConfiguration($model, $where);

        if ($get) {
            return $model->where($field, "LIKE", $value)->select($select)->get();
        }
        $return = $model->where($field, "LIKE", $value)->select($select)->take($items)->skip($skip)->get();
        return $return;

    }


    public static function findItem($where = null, $with = null, $select = "*", $scope = null)
    {
        $class = get_called_class();
        $model = $modelDefault = new $class;

        $model = $modelDefault->getWithConfiguration($model, $with);
        $model = $modelDefault->getScopeConfiguration($model, $scope);
        $model = $modelDefault->getWhereConfiguration($model, $where);


        $model = $model->select($select)->first();
        if ($model) {
            return $model->toArray();
        }
    }

    public static function findItemById($key, $with = null, $select = "*", $scope = null)
    {
        $class = get_called_class();
        $model = $modelDefault = new $class;
        $primaryKey = $modelDefault->primaryKey;

        $model = $modelDefault->getScopeConfiguration($model, $scope);
        $model = $modelDefault->getWithConfiguration($model, $with);
        $result = $model->where($primaryKey, "=", $key)->select($select)->first();

        if ($result != null) {
            return $result->toArray();
        } else {
            return $result;
        }
    }

    public static function findItemsById($keys, $with = null, $select = "*", $scope = null, $take = null)
    {

        $class = get_called_class();
        $model = $modelDefault = new $class;
        $primaryKey = $modelDefault->primaryKey;

        $model = $modelDefault->getScopeConfiguration($model, $scope);
        $model = $modelDefault->getWithConfiguration($model, $with);

        $key2 = implode(',', $keys);

        if ($with != null) {
            return $model->with($with)->whereIn($primaryKey, $keys)->orderByRaw(\DB::raw("FIELD($primaryKey, $key2)"))->take($take['take'])->skip($take['skip'])->get()->toArray();
        } else {
            return $model->whereIn($primaryKey, $keys)->orderByRaw(\DB::raw("FIELD($primaryKey, $key2)"))->take($take['take'])->skip($take['skip'])->get();
        }
    }


    public static function getModel($type, $fieldOrder = "id", $typeOrder = "asc", $items = 10, $where = null, $scope = null, $with = null, $select = "*", $whereIn = null)
    {
        $class = get_called_class();
        $model = $modelDefault = new $class;

        $model = $modelDefault->getWithConfiguration($model, $with);
        $model = $modelDefault->getScopeConfiguration($model, $scope);
        $model = $modelDefault->getWhereConfiguration($model, $where);
        $model = $modelDefault->getWhereInConfiguration($model, $whereIn);

        if ($typeOrder === "rand") {
            $model = $model->inRandomorder();
        } else {
            $model = $model->orderBy($fieldOrder, $typeOrder);
        }

        if (!is_array($select)) {
            $select = explode(",", $select);
        }

        switch ($type) {
            case"count":
                return $model->count();
                break;
            case"sql":
                return $model->toSql();
                break;
            case"array":
                return $model->get($select)->toArray();
                break;
            case"scoreboard":
                return $model->lists("label", "count");
                break;
            case"lists_name":
                return $model->lists("name", "id");
                break;
            case"lists_name_code":
                return $model->lists("name", "code");
                break;
            case"lists_title":
                return $model->lists("title", "id");
                break;
            case"lists_slug":
                return $model->lists("slug", "id");
                break;
            case"lists_label":
                return $model->lists("label", "id");
                break;
            case"lists_subject":
                return $model->lists("subject", "id");
                break;
            case"lists_id":
                return $model->lists("id");
                break;
            case "get":
                return $model->get()->toArray();
                break;
            case "nested":
                return $model->getNested();
                break;
            case "take":
                if (is_array($items)) {
                    $model = $model->take($items["take"])->skip($items["skip"]);
                } else {
                    $model = $model->take($items);
                }
                return $model->get($select)->toArray();
            case "paginate":
                if (is_array($items)) {
                    return $model->paginate($items["take"],$items["skip"]);
                } else {
                    return $model->paginate($items);
                }
                break;
        }
    }

    public function getWhereInConfiguration($model, $whereIn)
    {
        if ($whereIn) {
            if (count($whereIn) == 2) {
                $model = $model->whereIn($whereIn[0], $whereIn[1]);
            } else {
                $model = $model->whereIn('id', $whereIn[0]);
            }
        }

        return $model;
    }

    public function getWithConfiguration($model, $with)
    {
        if ($with) {
            $model = $model->with($with);

        }
        return $model;
    }

    public function getWhereConfiguration($model, $where)
    {
        if ($where) {
            if (is_array($where)) {
                $model = $model->whereRaw($where[0], $where[1]);

            } else {
                $model = $model->whereRaw($where);
            }
        }
        return $model;
    }

    public function getScopeConfiguration($model, $scope)
    {
        if ($scope) {
            if (is_array($scope)) {
                foreach ($scope as $s) {
                    if (is_array($s)) {
                        if ($s[1]!=null) {
                            $model = $model->{$s[0]}($s[1]);
                        }
                    } else {
                        $model = $model->{$s}();
                    }
                }
            } else {
                $model = $model->{$scope}();
            }
        }
        return $model;
    }

    public static function getRules()
    {
        $class = get_called_class();
        $model = $modelDefault = new $class;
        return $model->rules;
    }


}