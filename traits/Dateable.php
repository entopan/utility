<?php namespace Entopancore\Utility\Traits;


use Jenssegers\Date\Date;

/**
 * Created by PhpStorm.
 * User: annaubaldino
 * Date: 27/01/17
 * Time: 16:43
 */
trait Dateable
{

    public $mandatoryDates = ["short_date", "long_date"];

    public static function bootDateable()
    {

        if (!property_exists(get_called_class(), 'dateable')) {
            throw new \Exception(sprintf(
                'You must define a $dateable property in %s to use the Dateable trait.', get_called_class()
            ));
        }

        static::extend(function ($model) {
            foreach ($model->dateable['dates'] as $k=>$date)
            {
                $model->addDynamicMethod('get' . ucfirst(camel_case($k)) . 'Attribute', function () use ($model, $date) {
                    if($model->{$date['field']})
                    {
                        $result = Date::createFromTimeString($model->{$date['field']})->format($date['format']);
                        if(isset($date['capital_letter']))
                        {
                            return ucwords($result);
                        }
                        return $result;
                    }
                    return null;
                });

                $model->append([$k]);
            }
        });
    }

}