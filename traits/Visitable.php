<?php namespace Entopancore\Utility\Traits;

use Entopancore\Urls\Models\Visitable as ModelVisitable;
use Exception;

trait Visitable
{
    public static function bootVisitable()
    {
        if (class_exists("Entopancore\Urls\Plugin")) {
            static::extend(function ($model) {
                if (class_exists("Entopancore\User\Plugin")) {
                    $model->morphToMany["visits"] = ['Entopancore\User\Models\User',
                        'name' => 'visitable',
                        'table' => 'entopancore_urls_visitable',
                        'pivot' => ['quantity'],
                    ];
                }
                $model->bindEvent('model.beforeDelete', function () use ($model) {
                    $model->beforeDeleteVisit();
                });
            });
            
        }
    }

    public function getVisitAttribute()
    {
        if (class_exists("Entopancore\Urls\Plugin")) {
            $quantity = 0;
            if ($this->visits) {
                foreach ($this->visits as $visit) {
                    $quantity += $visit->pivot->quantity;
                };
            }
            return $quantity;
        }

    }

    public function beforeDeleteVisit()
    {
        $visits = ModelVisitable::where('visitable_id', '=', $this->id)->where('visitable_type', '=', $this->modelName)->get();
        foreach ($visits as $visit) {
            ModelVisitable::find($visit->id)->delete();
        }
    }

    public static function getMostVisitedItemsScoreboard($value, $orderType, $take)
    {
        $class = get_called_class();
        $model = $modelDefault = new $class;

        $modeltable = $model->getTable();

        $visited = \Db::table($modeltable)
            ->join('entopancore_urls_visitable', $modeltable . '.id', '=', 'entopancore_urls_visitable.visitable_id')
            ->where('visitable_type', '=', $class)
            ->select(\Db::raw('sum(quantity) as count,' . $modeltable . '.' . $value . ' as label'))
            ->groupBy('label')
            ->orderBy("count", $orderType)
            ->take($take['take'])
            ->skip($take['skip'])
            ->get();
        $result = collect($visited)->lists('label')->toArray();

        return $result;
    }


    public static function getMostVisitedItemsScoreboardInArray($value, $orderType, $take, $ids)
    {
        $class = get_called_class();
        $model = $modelDefault = new $class;
        $modeltable = $model->getTable();
        $ids2 = implode(',', $ids);

        $visited = \Db::table($modeltable)
            ->join('entopancore_urls_visitable', $modeltable . '.id', '=', 'entopancore_urls_visitable.visitable_id')
            ->where('visitable_type', '=', $class)
            ->whereIn('visitable_id', $ids)
            ->select(\Db::raw('sum(quantity) as count,' . $modeltable . '.' . $value . ' as label'))
            ->groupBy('label')
            ->orderByRaw(\DB::raw("FIELD(visitable_id, $ids2)"))
            ->orderBy("count", $orderType)
            ->take($take['take'])
            ->skip($take['skip'])
            ->get();
        $result = collect($visited)->lists('label')->toArray();
        return $result;
    }


    public static function getMostVisitedItems($with = null, $orderType, $select = "*", $scope = null, $take = null)
    {
        $visitedIds = self::getMostVisitedItemsScoreboard('id', $orderType, $take);
        if ($visitedIds) {
            $visited = self::findItemsById($visitedIds, $with, null, null, $take);
        } else {
            $visited = null;
        }

        return $visited;
    }

    public static function getMostVisitedItemsInArray($with = null, $orderType, $select = "*", $scope = null, $take = null, $ids)
    {
        $visitedIds = self::getMostVisitedItemsScoreboardInArray('id', $orderType, $take, $ids);

        if ($visitedIds) {

            $visited = self::findItemsById($visitedIds, $with, null, null, $take);
        } else {
            $visited = null;
        }

        return $visited;
    }

}
