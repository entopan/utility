<?php namespace Entopancore\Utility\Traits;

use Exception;
use Entopancore\Like\Models\Like;

trait Likable
{
    public static function bootLikable()
    {

        if (class_exists("Entopancore\Like\Plugin")) {
            static::extend(function ($model) {
                $model->morphToMany["likes"] = ['Entopancore\User\Models\User', 'name' => 'likable', 'table' => 'entopancore_like_likes'];
                $model->morphToMany["likes_count"] = ['Entopancore\User\Models\User', 'name' => 'likable', 'table' => 'entopancore_like_likes', 'count' => true];
                $model->bindEvent('model.beforeDelete', function () use ($model) {
                    $model->beforeDeleteLike();
                });
            });

        }
    }

    public function getLikeAttribute()
    {
        if (class_exists("Entopancore\Like\Plugin")) {
            return Like::where('likable_id', '=', $this->id)->where('likable_type', '=', get_called_class())->count();
        }
    }


    public function beforeDeleteLike()
    {
        $likes = Like::where('likable_id', '=', $this->id)->where('likable_type', '=', $this->modelName)->get();
        foreach ($likes as $like) {
            Like::find($like->id)->delete();
        }
    }

    public static function getMostLikedItemsScoreboard($value, $orderType, $take)
    {
        $class = get_called_class();
        $model = $modelDefault = new $class;
        $modelTable = $model->getTable();


        $liked = \Db::table($modelTable)
            ->join('entopancore_like_likes', $modelTable . '.id', '=', 'entopancore_like_likes.likable_id')
            ->where('likable_type', '=', $class)
            ->select(\Db::raw('count(*) as count,' . $modelTable . '.' . $value . ' as label'))
            ->groupBy('label')
            ->orderBy("count", $orderType)
            ->take($take['take'])
            ->skip($take['skip'])
            ->get();
        $result = collect($liked)->lists('label')->toArray();
        return $result;
    }

    public static function getMostLikedItemsScoreboardInArray($value, $orderType, $take, $ids)
    {
        $class = get_called_class();
        $model = $modelDefault = new $class;
        $modeltable = $model->getTable();
        $ids2=implode(',',$ids);

        $liked = \Db::table($modeltable)
            ->join('entopancore_like_likes', $modeltable . '.id', '=', 'entopancore_like_likes.likable_id')
            ->where('likable_type', '=', $class)
            ->whereIn('likable_id',$ids)
            ->select(\Db::raw('count(*) as count,' . $modeltable . '.' . $value . ' as label'))
            ->groupBy('label')
            ->orderByRaw(\DB::raw("FIELD(likable_id, $ids2)"))
            ->orderBy("count", $orderType)
            ->take($take['take'])
            ->skip($take['skip'])
            ->get();
        $result = collect($liked)->lists('label')->toArray();
        return $result;
    }


    public static function getMostLikedItems($with = null, $orderType, $select = "*", $scope = null, $take = null)
    {
        $likedIds = self::getMostLikedItemsScoreboard('id', $orderType, $take);
        if($likedIds)
        {
            $liked = self::findItemsById($likedIds,$with,null,null, $take);
        }
        else{
            $liked=null;
        }

        return $liked;
    }

    public static function getMostLikedItemsInArray($with = null, $orderType, $select = "*", $scope = null, $take = null,$ids)
    {
        $likedIds = self::getMostLikedItemsScoreboardInArray('id', $orderType, $take, $ids);
        if($likedIds)
        {
            $liked = self::findItemsById($likedIds,$with,null,null, $take);
        }
        else
        {
            $liked=null;
        }

        return $liked;
    }
}
