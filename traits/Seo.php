<?php namespace Entopancore\Utility\Traits;

use Schema;

trait Seo
{


    public static function bootSeo()
    {

        \Event::listen('backend.form.extendFieldsBefore', function ($widget) {

            $class = get_called_class();
            if (!$widget->model instanceof $class) {
                return;
            }
            if (get_parent_class($widget->model) != "October\Rain\Database\Model") {
                return;
            }

            if ($widget->context != "update") {
                return;
            }
            if (!\Schema::hasColumns($widget->model->table, ['seo_title', 'seo_description'])) {
                \Schema::table($widget->model->table, function ($table) {
                    $table->string('seo_title');
                    $table->text('seo_description');
                });
            }

            $widget->tabs["fields"]['seo_title'] = [
                'label' => 'Seo title',
                'placeholder' => 'Seo title',
                'type' => 'text',
                'tab' => 'Seo',
            ];
            $widget->tabs["fields"]['seo_description'] = [
                'label' => 'Seo description',
                'placeholder' => 'Seo description',
                'attributes' => ['maxlength' => 160],
                'type' => 'textarea',
                'tab' => 'Seo'
            ];

        });
    }
}
